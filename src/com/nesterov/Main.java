package com.nesterov;

public class Main {

    public static void main(String[] args) {
        int[] arr = {64, 34, 25, 12, 22, 11, 90};
        int n = arr.length;

        // Перехаодим по всем элементам массива.
        for (int i = 1; i < n; i++) {
            int key = arr[i];
            int j = i - 1;

            // Сдвигаем элементы больше ключа на одну позицию вправо.
            while (j >= 0 && arr[j] > key) {
                arr[j + 1] = arr[j];
                j--;
            }
            // Вставляем ключ в отсорториванную часть массива.
            arr[j + 1] = key;
        }
        // Выводим отсортированный массив.
        for (int i = 0; i < n; i++) {
            System.out.println(arr[i] + " ");
        }
    }
}
